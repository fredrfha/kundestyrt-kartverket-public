#!/bin/bash

# Convert notebooks to Python scripts, excluding code/notebooks/, and save the list of converted files
converted_files=$(find code -name "*.ipynb" ! -path "code/notebooks/*" -exec jupyter nbconvert --to script {} \; -print | sed 's/.ipynb$/.py/')

# Format the generated Python scripts using autopep8
echo "$converted_files" | xargs autopep8 --in-place

# Lint the formatted Python scripts outside of code/notebooks/ and existing .py files inside code/helpers/
lint_files=$(echo "$converted_files" | grep -v "code/notebook/")
pylint --disable=similarities $lint_files $(find ./code/helpers -name "*.py")

# Cleanup only the generated .py files
echo "$converted_files" | xargs rm
