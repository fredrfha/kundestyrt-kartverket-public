## SETUP

Python version: 3.8+

## Important information about the public project

In this project we have removed files that are not supposed to be public. This inludes raw data-files, which originally was placed in `/data/`. This means that you will be able to view, but not be able to run any notebooks unless you have acces to the raw data.

---

### virtual environment

Not necessary, but recommended.

From repository directotory `/`, create venv (virtual environment)

```
python -m venv venv
```

If this does not work you need to install `venv`

Activate venv with

```
source venv/bin/activate
```

All dependency-installations will now only be available in the virtual environmanet

---

### install dependencies

All necessary should be listed in `requirements.txt`. Install them with:

```
pip install -r requirements.txt
```

### Notebook stripping

To activate automatic stripping of notebook outputs, run:

```
nbstripout --install --attributes .gitattributes
```

---

### Repository Structure

The directory `/code`, contains all the code.

`/code/helpers` holds different helper functions used in several of the models

`/code/models` holds the models of our project.

- #### corrolation_matrix
- #### cvae_28x28
- #### cvae_800x1
- #### cvae_kmeans
- #### cvae_kmeans_remove_comets

`/code/notebook` holds notebooks and visualisation tools from Kartverket. The notebooks helps understanding and visualizing the different data.

- #### 1_read_kmall
- #### 2_explore_xarray
- #### 3_read_xarray

In repository directory `/data`, all the data from the project is placed (removed from this public repository).

The data is separated into `/data/initial-data` and `/data/new-data`, the first one is the data we got from Kartverket at the beginning of the project and the second is the data we ended up using in our models.

---

### Model descriptions

- #### Corrolation_Matrix
  This file displays a corrolation matrix of all the features in a zarr-file
- #### CVAE_28x28
  This model is based on a TensorFlow shell using the mnist dataset, and the pings were formatted into 28x28 images used in the CVAE model
- #### CVAE_800x1
  This model takes an entire ping and uses as input, making the images in latent space of the model visualized by graphs of the following pings
- #### CVAE_KMeans
  This model uses input shape 800x3, 3 pings as input, and the latent space is classified into clusters to find pings that are similar to eachother. To do this we used K-Means.
- #### CVAE_Remove_Comets_Kmeans_Example
  This model uses KMeans to find errors in the data and remove these from the dataset. Also this Notebook contains some plottings of the data, both before and after removal. It stores gifs of 3d-plots before and after removal in folder `./code/models/gifs`.

#### All these notebooks needs to be run from top to bottom to visualize all the plots, latent spaces and kmeans clusters in the respective models.

---
