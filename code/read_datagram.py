# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 08:56:27 2021

@author: sunhen
"""
import kmall_ext as kmall

import yaml
import string

from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
    
#kmall_file = r'C:\local\data\kmall_data\0007_20190513_154724_ASVBEN.kmall'
kmall_sample_file = r'Z:\Produktomrader\Nautilus\utvikling\ntnu\dof-2019-nh03-b07\RAW\EM304\0000_20191008_081150.kmall'
datetime_fmt = "%Y %m %d, %H:%M:%S.%f"


def prep_datagram(kmall_obj, dg_name):
    print("prepping index for", dg_name)
    
    dg_info = kmall_obj.Index['MessageType'] == "b'#"+dg_name+"'"
    print('dg_info', dg_info)
    dg_index = kmall_obj.Index[dg_info]
    print('dg_index', dg_index)
    dg_index.head()
    kmall_obj.OpenFiletoRead()
    kmall_obj.FID.seek(dg_index["ByteOffset"].iloc[0],0)
    #return kmall_obj

def read_kmall_datagrams(kmall_file):
    K = kmall.kmall(kmall_file)
    K.index_file()
    print(K.Index.iloc[0:20,:])
    K.report_packet_types()
    result = K.check_ping_count()
        
    prep_datagram(K, 'MRZ')
    dg = K.read_EMdgmMRZ()
    
    print("MRZ Records:  " + ",".join( dg.keys()))
    print("Soundings Record Fields: " + ",\n\t".join(dg["sounding"].keys()))
    
    #K.read_EMdgmMRZ_sounding()
    
    pc = K.check_ping_count()
    inline_dim = pc[1]+pc[2]
    crossline_dim = dg['rxInfo']['numSoundingsMaxMain']
    date_time = dg['header']['dgdatetime']
    # iSPO = K.Index['MessageType'] == "b'#SPO'"
    # SPOIndex = K.Index[iSPO]
    
    # Extract the nav data
    K.extract_nav()
    nav_points = len(K.att['datetime'])
    return dg, inline_dim, crossline_dim, date_time, nav_points

def read_ping_header(ping_no):
    
    offset = MRZIndex["ByteOffset"].iloc[ping_no]
    print('offset :', offset)
    K.FID.seek(offset,0)
    dg = K.read_EMdgmMRZ()
    header = dg['header']
    #print('header time', dg['header'])
    return header['dgdatetime']
    
def read_ping(ping_no, sector):
    
    offset = MRZIndex["ByteOffset"].iloc[ping_no]
    print('offset :', offset)
    K.FID.seek(offset,0)
    dg = K.read_EMdgmMRZ()
    sounding = dg['soundings']
    print('header time', dg['header'])
    print('z - value', sounding['z_reRefPoint_m'][sector])
    print('echo_Length_sec', sounding['echo_Length_sec'][sector])
    print('soundingIndex', sounding['soundingIndex'][sector])
    
    return sounding

def define_config_file(kmall_file, inline_dim, xline_dim, date_time, pos_points):
    # Write the dimension data to a config file:
    kmall_zarr_d = {}
    kmall_zarr_d['filename'] = kmall_file
    kmall_zarr_d['kmall']  = {}
    kmall_zarr_d['kmall']['MRZ'] = {}
    mrz_dg = kmall_zarr_d['kmall']['MRZ']
    mrz_dg['datagram_type'] = 'MRZ'
    mrz_dg['datagram_name'] = 'EM_DGM_M_RANGE_AND_DEPTH'
    mrz_dg['dimensions'] = {}
    mrz_dg['dimensions']['ndim'] = 2
    mrz_dg['dimensions'][1] = {}
    mrz_dg['dimensions'][1]['dim_name'] = 'inline'
    mrz_dg['dimensions'][1]['dim_length'] = int(inline_dim)
    mrz_dg['dimensions'][2] = {}
    mrz_dg['dimensions'][2]['dim_name'] = 'xline'
    mrz_dg['dimensions'][2]['dim_length'] = int(xline_dim)
    mrz_dg['variables'] = {}
    mrz_dg['variables']['soundingIndex'] = {}
    mrz_dg['variables']['soundingIndex']['type'] = 'float'
    mrz_dg['variables']['soundingIndex']['var_name'] = 'index'
    mrz_dg['variables']['x_reRefPoint_m'] = {}
    mrz_dg['variables']['x_reRefPoint_m']['type'] = 'float'
    mrz_dg['variables']['x_reRefPoint_m']['var_name'] = 'x'
    mrz_dg['variables']['y_reRefPoint_m'] = {}
    mrz_dg['variables']['y_reRefPoint_m']['type'] = 'float'
    mrz_dg['variables']['y_reRefPoint_m']['var_name'] = 'y'
    mrz_dg['variables']['z_reRefPoint_m'] = {}
    mrz_dg['variables']['z_reRefPoint_m']['type'] = 'float'
    mrz_dg['variables']['z_reRefPoint_m']['var_name'] = 'z'
    mrz_dg['variables']['reflectivity1_dB'] = {}
    mrz_dg['variables']['reflectivity1_dB']['type'] = 'float'
    mrz_dg['variables']['reflectivity1_dB']['var_name'] = 'bs_1'
    mrz_dg['variables']['z_reRefPoint_m']['var_name'] = 'z'
    mrz_dg['variables']['reflectivity2_dB'] = {}
    mrz_dg['variables']['reflectivity2_dB']['type'] = 'float'
    mrz_dg['variables']['reflectivity2_dB']['var_name'] = 'bs_2'
    mrz_dg['variables']['beamAngleReRx_deg'] = {}
    mrz_dg['variables']['beamAngleReRx_deg']['type'] = 'float'
    mrz_dg['variables']['beamAngleReRx_deg']['var_name'] = 'angle'
    mrz_dg['variables']['qualityFactor'] = {}
    mrz_dg['variables']['qualityFactor']['type'] = 'float'
    mrz_dg['variables']['qualityFactor']['var_name'] = 'qF'
    mrz_dg['variables']['detectionUncertaintyVer_m'] = {}
    mrz_dg['variables']['detectionUncertaintyVer_m']['type'] = 'float'
    mrz_dg['variables']['detectionUncertaintyVer_m']['var_name'] = 'tuv'
    mrz_dg['variables']['detectionUncertaintyHor_m'] = {}
    mrz_dg['variables']['detectionUncertaintyHor_m']['type'] = 'float'
    mrz_dg['variables']['detectionUncertaintyHor_m']['var_name'] = 'tuh'
    mrz_dg['variables']['twoWayTravelTime_sec'] = {}
    mrz_dg['variables']['twoWayTravelTime_sec']['type'] = 'float'
    mrz_dg['variables']['twoWayTravelTime_sec']['var_name'] = 'travel_time'
    mrz_dg['variables']['detectionWindowLength_sec'] = {}
    mrz_dg['variables']['detectionWindowLength_sec']['type'] = 'float'
    mrz_dg['variables']['detectionWindowLength_sec']['var_name'] = 'det_winlen'
    mrz_dg['variables']['detectionUncertaintyVer_m'] = {}
    mrz_dg['variables']['detectionUncertaintyVer_m']['type'] = 'float'
    mrz_dg['variables']['detectionUncertaintyVer_m']['var_name'] = 'det_veru'
    mrz_dg['variables']['detectionUncertaintyHor_m'] = {}
    mrz_dg['variables']['detectionUncertaintyHor_m']['type'] = 'float'
    mrz_dg['variables']['detectionUncertaintyHor_m']['var_name'] = 'det_horu'
    mrz_dg['variables']['deltaLatitude_deg'] = {}
    mrz_dg['variables']['deltaLatitude_deg']['type'] = 'float'
    mrz_dg['variables']['deltaLatitude_deg']['var_name'] = 'delta_lat_deg'
    mrz_dg['variables']['deltaLongitude_deg'] = {}
    mrz_dg['variables']['deltaLongitude_deg']['type'] = 'float'
    mrz_dg['variables']['deltaLongitude_deg']['var_name'] = 'delta_lon_deg'
    
    mrz_dg['datetime'] = date_time.strftime(datetime_fmt)
    mrz_dg['datetime_fmt'] = datetime_fmt
    
    kmall_zarr_d['kmall']['Ping'] = {}
    mrz_ping_dg = kmall_zarr_d['kmall']['Ping']
    mrz_ping_dg['datagram_type'] = 'Ping'
    mrz_ping_dg['datagram_name'] = 'Ping info'
    mrz_ping_dg['dimensions'] = {}
    mrz_ping_dg['dimensions']['ndim'] = 1
    mrz_ping_dg['dimensions'][1] = {}
    mrz_ping_dg['dimensions'][1]['dim_name'] = 'inline'
    mrz_ping_dg['dimensions'][1]['dim_length'] = int(inline_dim)
    mrz_ping_dg['variables'] = {}
    mrz_ping_dg['variables']['datetime'] = {}
    mrz_ping_dg['variables']['datetime']['var_name'] = 'pingtime'
    mrz_ping_dg['variables']['datetime']['type'] = 'datetime64[us]'
    # mrz_ping_dg['variables']['sort_index'] = {}
    # mrz_ping_dg['variables']['sort_index']['var_name'] = 'sortindex'
    # mrz_ping_dg['variables']['sort_index']['type'] = 'int'
    mrz_ping_dg['variables']['latitude_deg'] = {}
    mrz_ping_dg['variables']['latitude_deg']['var_name'] = 'latitude_deg'
    mrz_ping_dg['variables']['latitude_deg']['type'] = 'float'
    mrz_ping_dg['variables']['longitude_deg'] = {}
    mrz_ping_dg['variables']['longitude_deg']['var_name'] = 'longitude_deg'
    mrz_ping_dg['variables']['longitude_deg']['type'] = 'float'
    mrz_ping_dg['variables']['z_waterLevelReRefPoint_m'] = {}
    mrz_ping_dg['variables']['z_waterLevelReRefPoint_m']['var_name'] = 'z_water_level'
    mrz_ping_dg['variables']['z_waterLevelReRefPoint_m']['type'] = 'float'
    
    kmall_zarr_d['kmall']['SPO'] = {}
    spo_dg = kmall_zarr_d['kmall']['SPO']
    spo_dg['datagram_type'] = 'SPO'
    spo_dg['datagram_name'] = 'EM_DGM_S_POSITION'
    spo_dg['dimensions'] = {}
    spo_dg['dimensions']['ndim'] = 1
    spo_dg['dimensions'][1] = {}
    spo_dg['dimensions'][1]['dim_name'] = 'time'
    spo_dg['dimensions'][1]['dim_length'] = pos_points
    spo_dg["variables"] = {}
    spo_dg['variables']['dgtime'] = {}
    spo_dg['variables']['dgtime']['type'] = 'float'
    spo_dg['variables']['dgtime']['var_name'] = 'epoch_time'
    # TODO change the variable name to nav_time
    spo_dg['variables']['datetime'] = {}
    spo_dg['variables']['datetime']['var_name'] = 'nav_datetime'
    spo_dg['variables']['datetime']['type'] = 'datetime64'
    spo_dg['variables']['latitude_deg'] = {}
    spo_dg['variables']['latitude_deg']['var_name'] = 'nav_lat'
    spo_dg['variables']['latitude_deg']['type'] = 'float'
    spo_dg['variables']['longitude_deg'] = {}
    spo_dg['variables']['longitude_deg']['var_name'] = 'nav_lon'
    spo_dg['variables']['longitude_deg']['type'] = 'float'
    spo_dg['variables']['ellipsoidHeight_m'] = {}
    spo_dg['variables']['ellipsoidHeight_m']['var_name'] = 'ellipsoidHeight_m'
    spo_dg['variables']['ellipsoidHeight_m']['type'] = 'float'
    spo_dg['variables']['roll_deg'] = {}
    spo_dg['variables']['roll_deg']['var_name'] = 'roll_deg'
    spo_dg['variables']['roll_deg']['type'] = 'float'
    spo_dg['variables']['pitch_deg'] = {}
    spo_dg['variables']['pitch_deg']['var_name'] = 'pitch'
    spo_dg['variables']['pitch_deg']['type'] = 'float'
    spo_dg['variables']['heading_deg'] = {}
    spo_dg['variables']['heading_deg']['var_name'] = 'heading'
    spo_dg['variables']['heading_deg']['type'] = 'float'
    
    # iSPO = K.Index['MessageType'] == "b'#SPO'"
    # SPOIndex = K.Index[iSPO]
    # SPOIndex
    # SPOIndex.head()
    # K.OpenFiletoRead()
    #K.FID.seek(SPOIndex["ByteOffset"].iloc[0],0)
    
    kmall_conf = '\\'.join(kmall_file.split('\\')[:-1])+'\\conf\\'+kmall_file.split('\\')[-1]+'_conf'
    config_file = open(kmall_conf,'w')
    
    config_file.write(dump(kmall_zarr_d, Dumper=Dumper))
    config_file.close()
    return kmall_conf

def read_datagram_headers(kmall_file):
    dg, inline_dim, crossline_dim, date_time, nav_points = read_kmall_datagrams(kmall_file)
    return dg, inline_dim, crossline_dim, date_time, nav_points
    
def create_config_file(kmall_file, inline_dim, crossline_dim, date_time, nav_points):
    config_file =  define_config_file(kmall_file, inline_dim, crossline_dim, date_time, nav_points)
    return config_file

if __name__ == '__main__':
    dg,i,x,dt,navs = read_datagram_headers(kmall_sample_file)
    config_file = define_config_file(kmall_sample_file,i,x,dt,navs)