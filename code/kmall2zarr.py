# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 09:27:58 2023

@author: sunhen
"""

""" A script to read a folder with .kmall files and convert the data into xarray/zarr format
"""

import os
import xarray as xr

kmall_folder =  r'Z:\Produktomrader\Nautilus\utvikling\ntnu\dof-2019-nh03-b07\RAW\EM304'
zarr_folder = r'Z:\Produktomrader\Nautilus\utvikling\ntnu\dof-2019-nh03-b07\RAW\zarr'

from read_datagram import read_datagram_headers, create_config_file
from read_yaml import read_kmall_header, kmall_xarray, fix_timings, read_kmall_dt


def convert_kmall_to_zarr(dir_entry):
    kmall_file = dir_entry.path
    print(f' Reading from file: {kmall_file}')
    
    try:
        assert(kmall_file[-5:] == 'kmall')
    except AssertionError:
        print(f'{kmall_file} is not a kmall file, skipping...')
        return
        
        
    """ 1. Read the kmall file
    """
    dg,inline_dim, xline_dim, dt, nav_points = read_datagram_headers(kmall_file)
 
    
    """ 2. Write the config file which defines the xarray structure
    """
    config_file = create_config_file(kmall_file, inline_dim, xline_dim, dt, nav_points)
    print(f'Config file: {config_file}')
    
    
    """ 3. Load the config file
    """
    xarray_defs = read_kmall_dt(config_file)
    
    
    """ 4. Convert the datagrams to xarray
    """
    xarray_datagrams = kmall_xarray(xarray_defs)
    
    
    """ 5. Create a xarray dataset object
    """
    dataset = xr.Dataset(xarray_datagrams)
    
    
    """ 5. Do some cleanup on the datagram sequence
    """
    
    
    fixed_dataset = fix_timings(dataset)
    
    """ 6. Store the xarray object to zarr
    """
    zarr_file = zarr_folder + '\\' + dir_entry.name[:-6] + '.zarr'
    fixed_dataset.to_zarr(zarr_file, mode='w')
    
    
    
#def read_dataset_pipeline(dir_entry):
#    return (convert_kmall_to_zarr(file_i) for file_i in file_list)


if __name__ == '__main__':
    kmall_files = os.scandir(kmall_folder)
    
    temp_list = []
    for f_temp in kmall_files:
        temp_list.append(f_temp)
    
    for kmall_file in temp_list:
        convert_kmall_to_zarr(kmall_file)
    
#    for kmall_file in kmall_files[0:2]:
#        convert_kmall_to_zarr(kmall_file)
        #print(kmall_file)
