import numpy as np
from sklearn.cluster import KMeans

def get_smaller_cluster_indices(latent_representations: np.array, all_ping_indices: list) -> list:
    """
    Identify the smaller cluster in the latent representations using K-means clustering and return the 
    full two-element lists from all_ping_indices of the inputs belonging to that cluster.

    Parameters:
    - latent_representations: Array containing the latent points for each input.
    - all_ping_indices: 2D Array where each row contains the ping_index and an identifier/batch index.

    Returns:
    - List of two-element lists from all_ping_indices for the inputs in the smaller cluster.
    """
    
    # Perform K-means Clustering
    kmeans = KMeans(n_clusters=2, random_state=0).fit(latent_representations)
    
    # Identify the Smaller Cluster
    cluster_sizes = [sum(kmeans.labels_ == i) for i in range(2)]
    smaller_cluster_label = np.argmin(cluster_sizes)
    
    # Extract the full two-element list for the smaller cluster
    selected_ping_entries = [all_ping_indices[i] for i in range(len(all_ping_indices)) if kmeans.labels_[i] == smaller_cluster_label]
    
    return selected_ping_entries