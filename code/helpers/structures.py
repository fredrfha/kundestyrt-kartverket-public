from enum import Enum
from typing import NamedTuple


class Directory(Enum):
    NEW = "../../data/new-data/zarr3/"
    OLD = "../../data/initial-data/zarr/"
    CLEAN = "../../data/clean-data/zarr/"

    def __str__(self):
        return self.value


class TrainTestSets(NamedTuple):
    train: list
    test: list
