import os
from .structures import Directory, TrainTestSets
import numpy as np
import xarray as xr
import tensorflow as tf
from typing import Tuple

def get_zarr(directory, i=None):
    """
    Returns the path to the ith subdirectory from the specified Directory enum value, sorted alphabetically.
    If i is None, returns all subdirectories.

    :param directory: Either Directory.NEW or Directory.OLD.
    :param i: Index of the subdirectory to retrieve. If None, returns all subdirectories.
    :return: Full path to the ith subdirectory or a list of all subdirectory paths.
    """

    # Ensure directory is a valid Directory enum value
    if not isinstance(directory, Directory):
        raise ValueError(
            "Invalid directory type. Use Directory.NEW or Directory.OLD.")

    folder_path = directory.value
    subdirs = sorted([d for d in os.listdir(folder_path)
                     if os.path.isdir(os.path.join(folder_path, d))])

    # If i is None, return all subdirectories
    if i is None:
        return [os.path.join(folder_path, subdir) for subdir in subdirs]

    # Otherwise, check the index and return the ith subdirectory
    if i < 1 or i > len(subdirs):
        raise ValueError("Index out of range")

    return os.path.join(folder_path, subdirs[i - 1])


# Example usage:
# This will print the full path to the first subdirectory in the 'new-data/zarr3' folder, sorted alphabetically
# print(get_zarr(Directory.NEW, 1))


def split_train_test(directory: Directory, test_ratio=0.2, max_files=10, specific_test_idx=None):
    """
    Split the given directory into training and test sets.

    Parameters:
    - directory (str): The directory that needs to be split into train and test sets.
    - test_ratio (float): The ratio of the test set to the total number of files in the directory. Default is 0.2.
    - max_files (int): The maximum number of files to include in the train and test sets. Default is 10.
    - specific_test_idx (int): If provided, a specific index of a file to include in the test set. Default is None.

    Returns:
    - TrainTestSets: An object containing the paths of the training and test sets.

    Raises:
    - ValueError: If the specific_test_idx is out of range.
    """

    files = sorted(get_zarr(directory))
    files = files[:max_files]

    # Calculate the number of test files based on test_ratio and max_files
    num_test = round(test_ratio * max_files)
    num_train = max_files - num_test

    # If a specific test index is provided, use it for the test set
    if specific_test_idx is not None:
        if specific_test_idx < 0 or specific_test_idx >= len(files):
            raise ValueError(
                f"Specific test index '{specific_test_idx}' out of range.")

        test = [files[specific_test_idx]]
        train = [file_name for i, file_name in enumerate(
            files) if i != specific_test_idx]
    else:
        # Otherwise, split based on the calculated num_test and num_train
        test = files[:num_test]
        train = files[num_test:num_test + num_train]

    return TrainTestSets(train=train, test=test)

def single_file_to_tf_dataset(file_path: str, batch_size: int, input_shape=(3, 800)) -> tf.data.Dataset:
    xr_data = xr.open_zarr(store=file_path)
    images_with_index = gen_input_with_indicies(xr_data['z'].values.astype(np.float32), input_shape)
    
    images, indices = zip(*images_with_index)
    images_array = np.array(images)
    indices_array = np.array(indices)  # Convert indices to a numpy array
    
    return tf.data.Dataset.from_tensor_slices((images_array, indices_array)).batch(batch_size)

def gen_input_with_indicies(z_values, input_shape=(3, 800)):
    """
    Generate input images from a given array of z-values.

    Parameters:
    - z_values: numpy array representing the z-values.
    - input_shape: tuple representing the desired shape of the sub-image. Default is (3, 800).

    Returns:
    - List of tuples where each tuple contains a numpy array (the image) and an integer (the index).
    """

    small_images_with_index = []
    num_pings = z_values.shape[1]

    for i in range(0, num_pings - 2):
        sub_image = z_values[i:i+3, :]

        if sub_image.shape == input_shape:
            min_val = np.min(sub_image)
            max_val = np.max(sub_image)
            normalized_image = (sub_image - min_val) / (max_val - min_val)
            small_images_with_index.append((normalized_image, i+1))

    return small_images_with_index  # Return the list directly without converting to a numpy array

def gen_input(z_values, input_shape=Tuple):
    """
    Generate input images from a given array of z-values.

    Parameters:
    - z_values: numpy array representing the z-values.
    - input_shape: tuple representing the desired shape of the sub-image. Default is (800, 3).

    Returns:
    - numpy array containing the generated small images.

    Example Usage:
    z_values = np.random.rand(100, 800)
    input_shape = (800, 3)
    generated_images = gen_input_images(z_values, input_shape)
    """

    small_images = []

    # The number of pings
    num_pings = z_values.shape[1]
    print(z_values.shape)

    # Iterate over the pings, taking 3 pings at a time
    for i in range(0, num_pings - 2):
        # Extracts a subset of the z-values with 3 consecutive pings, each with 800 beams
        sub_image = z_values[i:i+3, :]

        # Ensure the sub-image has the desired size (800x3)
        if sub_image.shape == input_shape:
            # Normalize z-values in sub-image to values between 0 and 1
            min_val = np.min(sub_image)
            max_val = np.max(sub_image)
            normalized_image = (sub_image - min_val) / (max_val - min_val)

            small_images.append(normalized_image)

    return np.array(small_images)

