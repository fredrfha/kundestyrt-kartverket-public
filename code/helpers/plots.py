import matplotlib.pyplot as plt
from pathlib import Path
import xarray as xr
from pyproj import CRS
from pyproj import Transformer
import numpy as np
from IPython.display import Image
from matplotlib.animation import FuncAnimation
from matplotlib import cm
from matplotlib.ticker import LinearLocator

def scatter_plot(zarr_ds, transformer):
    # Transform the ping reference point to target crs
    ref_utm_x, ref_utm_y = transformer.transform(zarr_ds.latitude_deg, zarr_ds.longitude_deg)

    # Create xarray arrays for easier broadcast...
    x_xr = xr.DataArray(ref_utm_x,dims=['inline'])
    y_xr = xr.DataArray(ref_utm_y,dims=['inline'])

    # Add the reference point position (in meters) and the sounding position
    x_utm = x_xr + zarr_ds.x
    y_utm = y_xr + zarr_ds.y

    # Kan vi plotte punktene som scatter plot?
    plt.scatter(x_utm, y_utm, c=zarr_ds.z)
    plt.show()

def surface_plot(zarr_ds, transformer):
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    # Transform the ping reference point to target crs
    ref_utm_x, ref_utm_y = transformer.transform(zarr_ds.latitude_deg, zarr_ds.longitude_deg)

    # Create xarray arrays for easier broadcast...
    x_xr = xr.DataArray(ref_utm_x,dims=['inline'])
    y_xr = xr.DataArray(ref_utm_y,dims=['inline'])

    # Add the reference point position (in meters) and the sounding position
    x_utm = x_xr + zarr_ds.x
    y_utm = y_xr + zarr_ds.y

    # Plot the surface.
    surf = ax.plot_surface(x_utm, y_utm, zarr_ds.z, cmap=cm.coolwarm,
                        linewidth=0, antialiased=False)

    # Customize the z axis.
    zlims = ax.get_zlim()
    ax.set_zlim(zlims[::-1])


    ax.zaxis.set_major_locator(LinearLocator(10))
    # A StrMethodFormatter is used automatically
    ax.zaxis.set_major_formatter('{x:.02f}')

    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()

def surface_plot_gif(zarr_ds, transformer, gif_path):
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    # Transform the ping reference point to target crs
    ref_utm_x, ref_utm_y = transformer.transform(zarr_ds.latitude_deg, zarr_ds.longitude_deg)

    # Create xarray arrays for easier broadcast...
    x_xr = xr.DataArray(ref_utm_x,dims=['inline'])
    y_xr = xr.DataArray(ref_utm_y,dims=['inline'])

    # Add the reference point position (in meters) and the sounding position
    x_utm = x_xr + zarr_ds.x
    y_utm = y_xr + zarr_ds.y

    # Plot the surface.
    surf = ax.plot_surface(x_utm, y_utm, zarr_ds.z, cmap=cm.coolwarm,
                        linewidth=0, antialiased=False)

    # Customize the z axis.
    zlims = ax.get_zlim()
    ax.set_zlim(zlims[::-1])

    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter('{x:.02f}')
    
    # Add a color bar which maps values to colors
    fig.colorbar(surf, shrink=0.5, aspect=5)

    # Function to update the plot for each frame
    def update(frame):
        angle = frame % 360
        ax.view_init(30, angle)
        return fig,

    # Create an animation
    ani = FuncAnimation(fig, update, frames=range(1, 360, 5), interval=250, blit=False)

    # Save the animation as a GIF
    ani.save(gif_path, writer='pillow')

    plt.close(fig)
