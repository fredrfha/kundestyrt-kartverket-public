from typing import Tuple
import numpy as np


# Gammel metode ?
# tar inn alle 500x250 z-verdiene
# returner en liste av utsnitt av z-verdiene med shape definert i image_size
# def gen_input(z_values):
#     print(z_values)
#     small_images = []

#     # Iterate over the x-y grid and extract sub-images
#     for i in range(0, z_values.shape[0], input_shape[0]):
#         for j in range(0, z_values.shape[1], input_shape[1]):

#             # henter verdier for utsnitt, med shape i image_size
#             sub_image = z_values[i:i+input_shape[0], j:j+input_shape[1]]

#             # Ensure the sub-image has the desired size (28x28)
#             if sub_image.shape == input_shape:
#                 # normaliserer z-verdier i sub-image til verdier mellom 0 og 1, i stedet for f.eks verdier fra 2500-4000 (dybde)
#                 min_val = np.min(sub_image)
#                 max_val = np.max(sub_image)
#                 normalized_image = (sub_image - min_val) / (max_val - min_val)

#                 sub_image = np.expand_dims(normalized_image, axis=-1)
#                 small_images.append(sub_image)

#     return np.array(small_images)

def gen_input_images(z_values, input_shape=Tuple):
    """
    Generate input images from a given array of z-values.

    Parameters:
    - z_values: numpy array representing the z-values.
    - input_shape: tuple representing the desired shape of the sub-image. Default is (800, 3).

    Returns:
    - numpy array containing the generated small images.

    Example Usage:
    z_values = np.random.rand(100, 800)
    input_shape = (800, 3)
    generated_images = gen_input_images(z_values, input_shape)
    """

    small_images = []

    # The number of pings
    num_pings = z_values.shape[1]
    print(z_values.shape)

    # Iterate over the pings, taking 3 pings at a time
    for i in range(0, num_pings - 2):
        # Extracts a subset of the z-values with 3 consecutive pings, each with 800 beams
        sub_image = z_values[i:i+3, :]

        # Ensure the sub-image has the desired size (800x3)
        if sub_image.shape == input_shape:
            # Normalize z-values in sub-image to values between 0 and 1
            min_val = np.min(sub_image)
            max_val = np.max(sub_image)
            normalized_image = (sub_image - min_val) / (max_val - min_val)

            small_images.append(normalized_image)

    return np.array(small_images)