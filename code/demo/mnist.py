from IPython import display

import glob
import imageio
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import PIL
import tensorflow as tf
import tensorflow_probability as tfp
import time


def main():
    (train_images, _), (test_images, _) = tf.keras.datasets.mnist.load_data()
    plt.imshow(train_images[400])
    plt.show()
    pass

if __name__ == "__main__":
    main()