import xarray as xr
import zarr
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import medfilt

class Data():

    filepath = None
    line = None # a line of data as xarray object

    def __init__(self, filepath):

        self.filepath = filepath
        
        self.line = xr.open_zarr(store=filepath)
        for var_name in self.line:
            print(var_name)

    def visualize(self):
        x_axis = input("x-axis [default=x]: ")
        y_axis = input("x-axis [default=y]: ")

        plt.plot(self.line[x_axis if x_axis else 'x'], self.line[y_axis if y_axis else 'y'])
        # plt.show()
        plt.savefig('pyplot_figure.png')

    def visualize_smoothing(self, kernel_size=15):
        x_axis = input("x-axis [default=x]: ")
        y_axis = input("x-axis [default=y]: ")

        x_data = self.line[x_axis if x_axis else 'x']
        y_data = self.line[y_axis if y_axis else 'y']

        x_data = self.smoothing(x_data, kernel_size)
        y_data = self.smoothing(y_data, kernel_size)

        plt.plot(x_data, y_data)
        # plt.show()
        plt.savefig('pyplot_figure.png')

    def visualize_interpolated_y(self):
        x_axis = input("x-axis [default=x]: ")
        y_axis = input("x-axis [default=y]: ")
        x_data = self.line[x_axis if x_axis else 'x']
        y_data = self.line[y_axis if y_axis else 'y']

        y_data = self.interpolate(x_data, y_data)

        plt.plot(x_data, y_data)
        # plt.show()
        plt.savefig('pyplot_figure.png')

    def interpolate(self, list_to_keep, list_to_interpolate):
        interp_function = interp1d(np.linspace(0, len(list_to_keep) - 1, len(list_to_interpolate)), list_to_interpolate, kind='linear')
        interpolated_values = interp_function(np.arange(len(list_to_keep)))

        return interpolated_values
    
    def smoothing(self, list, kernel_size=9):
        return medfilt(list, kernel_size)

def main():
    data = Data(filepath="../../data/initial-data/zarr/0000_20191008_081150.zarr")
    data.visualize()
    # data.visualize_interpolated_y()
    # data.visualize_smoothing()

if __name__ == "__main__":
    main()



    


