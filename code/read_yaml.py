# -*- coding: utf-8 -*-
"""
Created on Tue Dec  7 15:10:30 2021

@author: sunhen
"""

"""
Read a yaml file and setup a zarr file structure
See https://github.com/valschmidt/kmall

"""
import logging
import numpy as np
import scipy

from datetime import datetime
import zarr
import xarray as xr
import dask
from matplotlib import pyplot as plt

import kmall_ext as kmall
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from pyproj import CRS
from pyproj import Transformer

logging.basicConfig(filename='C:\\local\\scripts\\OSCAR\\kmall\\kmall.log', level=logging.INFO)

kmall_catalog = "C:\\local\\data\\kmall_data\\conf"
conf_file = "0007_20190513_154724_ASVBEN.kmall_conf"

zarr_catalog = "C:\\local\\data\\zarr_data\\kmall2zarr\\foo.zarr"

def open_zarr():
    return xr.open_zarr(zarr_catalog)

def read_kmall_dt(yaml_file):
    """
    Reads the kmall datatype specification in the yaml file
    """
    #fp = open(kmall_catalog+'\\'+yaml_file)
    fp = open(yaml_file)
    return load(fp, Loader)

def read_ping(ping_no, sector):
    
    offset = MRZIndex["ByteOffset"].iloc[ping_no]
    print('offset :', offset)
    K.FID.seek(offset,0)
    dg = K.read_EMdgmMRZ()
    sounding = dg['soundings']
    print('header time', dg['header'])
    print('z - value', sounding['z_reRefPoint_m'][sector])
    print('echo_Length_sec', sounding['echo_Length_sec'][sector])
    print('soundingIndex', sounding['soundingIndex'][sector])
    
    return sounding

# Must be changed to be automated
def create_zarr(zarr_root, kmall_desc):
    command_list = []
    datagrams = kmall_desc['kmall']
    for dg in datagrams.keys():
        logging.info('create_zarr - def datagram, %s', dg)
        dataset = datagrams[dg]
        
        command_list.append(('create_group',zarr_root, dg))
        
        logging.info('create_zarr - zarr group %s', dg)
        
        dimensions = dataset['dimensions']
        n_dim = dimensions['ndim']
        logging.info('create_zarr - def dimensions %i', n_dim)
        # building the shape
        shp_l = [] 
        for dim in range(n_dim):
            logging.info('create_zarr - dimension number %i', dim)
            dim_length = int(dimensions[dim+1]['dim_length'])
            dim_name = dimensions[dim+1]['dim_name']
            logging.info('create_zarr - dimension number %i length', dim_length)
            logging.info('create_zarr - dimension number %s name', dim_name)
            shp_l.append(dimensions[dim+1])
        shp = tuple(shp_l)
        command_list.append(('create_array','/'+dg, shp, dim_name))
        


def read_kmall_header(kmall_filename, no_pings):
    npt = np.full(no_pings, fill_value=None, dtype='datetime64')
    npt = []
    kmall_obj = kmall.kmall(kmall_filename)
    kmall_obj.index_file()
    kmall_obj.check_ping_count()
    iMRZ = kmall_obj.Index['MessageType'] == "b'#MRZ'"
    MRZIndex = kmall_obj.Index[iMRZ]
    kmall_obj.OpenFiletoRead()
    for ping in range(no_pings):
        offset = MRZIndex["ByteOffset"].iloc[ping]
        kmall_obj.FID.seek(offset,0)
        dg = kmall_obj.read_EMdgmMRZ()
        date_time = dg['header']['dgdatetime']
        print('read_kmallvar - filename :', kmall_filename)
        print('                ping_no :', ping)
        print('                datetime :', date_time)
        npt.append(str(date_time))
    
    return [np.datetime64(x) for x in npt]
    
def read_soundingvar(kmall_filename, var_name, dims):
    npa = np.full(dims, fill_value=0.0, dtype='float')
    kmall_obj = kmall.kmall(kmall_filename)
    kmall_obj.index_file()
    kmall_obj.check_ping_count()
    iMRZ = kmall_obj.Index['MessageType'] == "b'#MRZ'"
    MRZIndex = kmall_obj.Index[iMRZ]
    kmall_obj.OpenFiletoRead()
    print('read_kmallvar - filename :', kmall_filename)
    print('                var_name :', var_name)
    print('                    dims :', dims)
    for inline in range(dims[0]):
        offset = MRZIndex["ByteOffset"].iloc[inline]
        kmall_obj.FID.seek(offset,0)
        dg = kmall_obj.read_EMdgmMRZ()
        sounding = dg['sounding']
        for xline in range(dims[1]):
            val = sounding[var_name][xline]
            npa[inline,xline] = val
    return npa

def read_navigationvar(kmall_filename, var_name, dims, dtype):
    npa = np.full(dims, fill_value= None, dtype=dtype)
    kmall_obj = kmall.kmall(kmall_filename)
    kmall_obj.extract_nav()
    npa = kmall_obj.att[var_name]
    return npa

# TODO finish this
def read_pinginfo(kmall_filename, var_name, dims, dtype):
    if dtype=='int':
        npa = np.full(dims, fill_value=0, dtype=dtype)
    else:
        npa = np.full(dims, fill_value=None, dtype=dtype)
    kmall_obj = kmall.kmall(kmall_filename)
    kmall_obj.index_file()
    kmall_obj.check_ping_count()
    iMRZ = kmall_obj.Index['MessageType'] == "b'#MRZ'"
    MRZIndex = kmall_obj.Index[iMRZ]
    kmall_obj.OpenFiletoRead()
    print('read_pinginfo - filename :', kmall_filename)
    print('                var_name :', var_name)
    print('                   dtype :', dtype)
    print('                    dims :', dims)
    for inline in range(dims[0]):
        offset = MRZIndex["ByteOffset"].iloc[inline]
        kmall_obj.FID.seek(offset,0)
        dg = kmall_obj.read_EMdgmMRZ()
        #print('                datetime :', dg['header']['dgdatetime'])
        if var_name == 'datetime':
            val = np.datetime64(dg['header']['dgdatetime'])
        else:
            val = dg['pingInfo'][var_name]
        npa[inline] = val
    return npa
        
    
    
# def kmall_to_zarr(kmall_f, zarr_obj):
#     kmall_obj = kmall.kmall(y_d['filename'])
#     kmall_obj.index_file()
#     kmall_obj.check_ping_count()
#     iMRZ = kmall_obj.Index['MessageType'] == "b'#MRZ'"
#     MRZIndex = kmall_obj.Index[iMRZ]
#     kmall_obj.OpenFiletoRead()
#     for p in range(zarr_obj.attrs['pings']):
#         print('ping no:', p)
#         offset = MRZIndex["ByteOffset"].iloc[p]
#         kmall_obj.FID.seek(offset,0)
#         sounding = kmall_obj.read_EMdgmMRZ()['soundings']
#         for b in range(zarr_obj.attrs['beams']):
#             x = sounding['x_reRefPoint_m'][b]
#             y = sounding['y_reRefPoint_m'][b]
#             z = sounding['z_reRefPoint_m'][b]
#             zarr_obj.x[p,b] = x
#             zarr_obj.y[p,b] = y
#             zarr_obj.z[p,b] = z
#             #print('beam no:', b,x,y,z)


def kmall_xarray(yaml_spec):
    kmall_filename = yaml_spec['filename']
    data_vars = {}
    # coords = {}
    # attributes = {}
    # data_arrays = []
    # Reads a MRZ datagram
    for datagram in yaml_spec['kmall'].keys():
        coords = {}
        attributes = {}
        data_arrays = []
        if datagram == 'MRZ':
            attributes['filename'] = yaml_spec['filename']
            dg = yaml_spec['kmall']['MRZ']
            dg_type = dg['datagram_type']
            attributes['datagram_type'] = dg['datagram_type']
            n_dim = dg['dimensions']['ndim']
            l_dim = []
            l_shape = []
            d_coords = {}
            for d in range(n_dim):
                l_dim.append(dg['dimensions'][d+1]['dim_name'])
                l_shape.append(dg['dimensions'][d+1]['dim_length'])
                coords = np.arange(dg['dimensions'][d+1]['dim_length'],dtype=int)
                d_coords[dg['dimensions'][d+1]['dim_name']] = coords
            #print('d_coords :', d_coords)
            #print('l_dim', l_dim)
            for var in dg['variables']:
                var_xarray = dg['variables'][var]['var_name']
                print('kmall variable', var,'xarray variable:',var_xarray)
                var_np = read_soundingvar(kmall_filename, var, tuple(l_shape))
                data_vars[var_xarray] = xr.DataArray(var_np, d_coords, l_dim, attrs=attributes)
        
        # Reads a SPO(?) datagram
        if datagram == 'SPO':
            attributes['filename'] = yaml_spec['filename']
            dg = yaml_spec['kmall']['SPO']
            attributes['datagram_type'] = dg['datagram_type']
            n_dim = dg['dimensions']['ndim']
            l_dim = []
            l_shape = []
            d_coords = {}
            for d in range(n_dim):
                l_dim.append(dg['dimensions'][d+1]['dim_name'])
                l_shape.append(dg['dimensions'][d+1]['dim_length'])
                coords = np.arange(dg['dimensions'][d+1]['dim_length'],dtype=int)
                d_coords[dg['dimensions'][d+1]['dim_name']] = coords
            #print('d_coords :', d_coords)
            #print('l_dim', l_dim)
            for var in dg['variables']:
                var_xarray = dg['variables'][var]['var_name']
                var_dtype = dg['variables'][var]['type']
                print('kmall variable', var,'xarray variable:',var_xarray,'datatype', var_dtype)
                var_np = read_navigationvar(kmall_filename, var, tuple(l_shape),var_dtype)
                data_vars[var_xarray] = xr.DataArray(var_np, d_coords, l_dim, attrs=attributes)
            # Reads a SPO(?) datagram
            
        if datagram == 'Ping':
            attributes['filename'] = yaml_spec['filename']
            dg = yaml_spec['kmall']['Ping']
            attributes['datagram_type'] = dg['datagram_type']
            n_dim = dg['dimensions']['ndim']
            l_dim = []
            l_shape = []
            d_coords = {}
            for d in range(n_dim):
                l_dim.append(dg['dimensions'][d+1]['dim_name'])
                l_shape.append(dg['dimensions'][d+1]['dim_length'])
                coords = np.arange(dg['dimensions'][d+1]['dim_length'],dtype=int)
                d_coords[dg['dimensions'][d+1]['dim_name']] = coords
            #print('d_coords :', d_coords)
            #print('l_dim', l_dim)
            for var in dg['variables']:
                var_xarray = dg['variables'][var]['var_name']
                var_dtype = dg['variables'][var]['type']
                print('kmall variable', var,'xarray variable:',var_xarray,'datatype', var_dtype)
                var_np = read_pinginfo(kmall_filename, var, tuple(l_shape),var_dtype)
                data_vars[var_xarray] = xr.DataArray(var_np, d_coords, l_dim, attrs=attributes)    
        
    return data_vars

# TODO fixing the pingtime?

def fix_timings(ds):
    # Do a reverse mapping to loop through
    var_d = {}
    for v in ds.variables:
        #print(ds[v].dims,ds[v].name)
        if 'inline' in ds[v].dims:
            if v not in ['inline','xline']:
                print(ds[v].dims,ds[v].name)
                var_d[ds[v].name] = ds[v].dims
    
    # Create the sorting index
    pingtime = ds['pingtime']
    pt_sorted = np.argsort(pingtime.to_numpy())
    sort = ds['pingtime'][pt_sorted] # sort = pingtime[npt]
    npa = np.arange(len(sort.inline))
    #sort2 = sort.assign_coords({'inline':npa.tolist()})
    
    # Take all the inline variables first
    #for inline_var in ('x','y','z'):
        
    for inline_var in var_d:
        print('fixing ',inline_var)
        temp_array = ds[inline_var][pt_sorted].assign_coords({'inline':npa})
        ds[inline_var][:] = temp_array
   
    # z_s = ds['z'][npa.tolist()].assign_coords({'inline':npa})
    # y_s = ds['y'][npa.tolist()].assign_coords({'inline':npa})
    # x_s = ds['x'][npa.tolist()].assign_coords({'inline':npa})
    # ds.to_zarr(zarr_store,mode='w')
    return ds
    
def create_new():
    y_d = read_kmall_dt(conf_file)
    dv = kmall_xarray(y_d)
    ds = xr.Dataset(dv)
#    ds.to_zarr(zarr_catalog)
    return ds

def to_latlon(ds):
    ds['yl'] = ds['latitude_deg']+ds['delta_lat_deg']
    ds['xl'] =  ds['longitude_deg']+ds['delta_lon_deg']
    ds['zm'] = ds['z'] - ds['z_water_level']
    return ds

def to_utm(ds):
    #EPSG 4258 is ETRS89
    #EPSG 5973 is UTM 33N
    from_crs = CRS.from_epsg(4258)
    to_crs = CRS.from_epsg(5973)
    trans = Transformer.from_crs(from_crs, to_crs)
    x,y = trans.transform(ds['xl'], ds['yl'])
    ds['x_m'] = xr.DataArray(x,ds['xl'].coords, ds['xl'].dims)
    ds['y_m'] = xr.DataArray(y,ds['yl'].coords, ds['yl'].dims)
    return ds



def smooth_it(ds):
    gridX,gridY = np.meshgrid(np.arange(300), np.arange(360))
    x_flat = ds['x_m'].to_numpy().reshape(240*400)
    y_flat = ds['y_m'].to_numpy().reshape(240*400)
    x_flat -=min(x_flat)
    y_flat -=min(y_flat)
    grid = scipy.interpolate.griddata(list(zip(x_flat,y_flat)), ds['zm'].to_numpy().reshape(240*400),(gridX,gridY))
    plt.imshow(grid)
    img = scipy.ndimage.gaussian_filter(grid, sigma=5)
    return img
    
def do_transform(x):
    return x+2

# Root mean square estimate?
def rms(array):
   return np.sqrt(np.mean(array ** 2))

# if __name__ == '__main__':
#     y_d = read_kmall_dt(conf_file)
#     root = zarr.open(zarr_catalog+'\\'+'foo'+'.zarr', mode='w')

# works...
# gridX,gridY = np.meshgrid(np.arange(300), np.arange(360))
# x_flat = ds['x_m'].to_numpy().reshape(240*400)
# y_flat = ds['y_m'].to_numpy().reshape(240*400)
# x_flat -=min(x_flat)
# y_flat -=min(y_flat)
# grid = scipy.interpolate.griddata(list(zip(x_flat,y_flat)), ds['zm'].to_numpy().reshape(240*400),(gridX,gridY))
# plt.imshow(grid)
# img = scipy.ndimage.gaussian_filter(grid, sigma=3)
# plt.contourf(img)
# plt.contour(img)
# works ...

#     img = scipy.ndimage.gaussian_filter(grid, sigma=5)
# plt.imshow()
# img
# grid
# grid = scipy.interpolate.griddata(list(zip(x_flat,y_flat)), ds['zm'].to_numpy().reshape(240*400),(gridX,gridY))
# plt.imshow()
# plt.imshow(grid)
# img = scipy.ndimage.gaussian_filter(grid, sigma=5)
# plt.imshow(img)
# plt.contour(img)
# img = scipy.ndimage.gaussian_filter(grid, sigma=3)
# plt.contour(img)
# plt.contourf(img)

# def rms(array):
#    return np.sqrt(np.mean(array ** 2))
# Calculating the delta between adjacent cells
# along x (second index)
# diff = a2[:,1:]-a1[:,:-1]
# along y (first index)


# diff = a2[1:,]-a1[:-1,:]
# y1 = ds['y_m'][1:,:]
# y2 = ds['y_m'][:-1,:]
#y_diff = xr.DataArray(y1.to_numpy()-y2.to_numpy())